# Nadi - Nasari Digital Test API Documentation

## Author

This project was created by

[Wisnu Nugraha](wisnunugraha@yahoo.com)

[gitlab](https://gitlab.com/wisnunugraha)

## Nadi - Nasari Digital - test

This repository contains the source code for the Nadi - Nasari Digital Test.

1. PHP - Laravel Framework
2. MySQL

## Installation

Use the package manager [composer](https://getcomposer.org/download/) to install package of Nadi - Nasari Digital - Test.

## Installation manual

```bash
# Intall package PHP
composer install

# Install package json
npm install

# Build asset vite
 npm run build

```

## create table nadidb db in MySQL

```bash
#Copy environment example to be .env
cp .env.example .env

#setup .env with same databse on local
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=nadidb
DB_USERNAME=root
DB_PASSWORD=
```

```bash
# Copy env
cp .env.example .env

# Generate Key
php artisan key:generate

# Migrate all table
php artisan migrate

# Run seed all run
php artisan db:seed

# Run seed only one
php artisan db:seed --class=UsersSeeder

# Run Server Laravel
php artisan serve
```

## open new browser

running http://localhost:8000

## License

[MIT](https://choosealicense.com/licenses/mit/)
