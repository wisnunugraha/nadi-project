<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('users')->insert([
            'fname' => 'test',
            'lname' => 'nadi',
            'email' => 'test@nadi.co.id',
            'password' => Hash::make('password'),
        ]);
    }
}
