<link rel="stylesheet" href="/login.css">
@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="d-flex justify-content-center h-100 w-70">
            <div class="card w-55" style="border-radius: none !important;">
                <div class="card-body " style="border-radius: none !important;">
                    <div class=" text-center">
                        <h3>Silahkan Login</h3>
                    </div>
                    <form method="POST" action="{{ route('login') }}">

                        @csrf
                        <div class="input-group form-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i data-feather="mail"></i></span>
                            </div>
                            <input type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"  placeholder="email" name="email">
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror

                        </div>
                        <div class="input-group form-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i data-feather="lock"></i></span>
                            </div>
                            <input type="password" class="form-control @error('password') is-invalid @enderror" placeholder="password" name="password">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="d-flex justify-content-center">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>
                                {{-- <input type="submit" value="Login" class="btn btn-primary login_btn"> --}}
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <script src="https://unpkg.com/feather-icons"></script>
    <script>
        feather.replace();
    </script>
@endsection
